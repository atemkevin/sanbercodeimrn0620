import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Button} from 'react-native';
import Icon from "react-native-vector-icons/AntDesign";

export default class LoginScreen extends Component {
    render(){
        return (
            <View>
                <View style={styles.profile}>
                    <Text style={styles.title}>About me</Text>
                    <Image source={require('./images/images.jpg')} style={styles.photo} />
                    <Text style={styles.name}>Kevin Claudio</Text>
                    <Text style={styles.job}>Programmer</Text>
                </View>
                <View style={styles.portofolio}>
                    <Text style={styles.titleSub}>Portofolio</Text>
                    <View style={{height:0.5, backgroundColor:'black'}}/>
                    <View style={styles.itemPortofolio}>
                        <View style={styles.git}>
                            <Icon name='gitlab' size={65} color='#3EC6FF'/>
                            <Text style={styles.titleLink}>atemkevin</Text>
                        </View>
                        <View style={styles.git}>
                            <Icon name='github' size={65} color='#3EC6FF'/>
                            <Text style={styles.titleLink}>atemkevin</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.sosmed}>
                    <Text style={styles.titleSub}>Hubungi Saya</Text>
                    <View style={{height:0.5, backgroundColor:'black'}}/>
                    <View style={styles.itemSosmed}>
                        <View style={styles.sosmedlist}>
                            <Icon name='facebook-square' size={65} color='#3EC6FF'/>
                            <Icon name='instagram' size={65} color='#3EC6FF'/>
                            <Icon name='twitter' size={65} color='#3EC6FF'/>
                            
                        </View>
                        <View style={styles.sosmedlist}>
                            <Text style={styles.titleSosmed}>Kevin Claudio</Text>
                            <Text style={styles.titleSosmed}>atemkevin</Text>
                            <Text style={styles.titleSosmed}>@atemkevin</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    profile:{
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20
    },
    title: {
        fontSize: 24,
        color: '#003366'
    },
    photo: {
        height: 150,
        width: 150,
        borderRadius: 150,
        marginVertical: 25
    },
    name: {
        fontSize: 20,
        color: '#003366'
    },
    job: {
        fontSize: 18,
        color: '#3EC6FF',
        marginTop: 10
    },
    portofolio: {
        backgroundColor: '#EFEFEF',
        marginHorizontal: 10,
        height: 150,
        borderRadius: 10,
        marginVertical: 10
    },
    titleSub: {
        fontSize: 16,
        marginVertical: 5,
        marginHorizontal: 10
    },
    itemPortofolio: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 15,
    },
    git: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    titleLink: {
        fontSize: 16,
        color: '#003366',
        marginTop: 5
    },
    sosmed: {
        backgroundColor: '#EFEFEF',
        marginHorizontal: 10,
        height: 275,
        borderRadius: 10,
        marginVertical: 10,
    },
    itemSosmed: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 15,
    },
    sosmedlist: {
        flexDirection: 'column',
        alignItems: 'center',
    },
    titleSosmed: {
        fontSize: 16,
        color: '#003366',
        marginVertical: 20,
        marginLeft: 20
    },
})
