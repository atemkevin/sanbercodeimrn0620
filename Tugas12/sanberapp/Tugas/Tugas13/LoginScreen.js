import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Button} from 'react-native';


export default class LoginScreen extends Component {
    render(){
        return (
            <View>
                <View style={styles.container}>
                    <Image source={require('./images/logo.png')} style={styles.Gambar}></Image>
                    <Text style={styles.Title}>Login</Text>
                </View>
                <Text style={styles.FormText}>Username</Text>
                <TextInput style={styles.Form}/>
                <Text style={styles.FormText}>Password</Text>
                <TextInput style={styles.Form}/>
                <View style={styles.container}>
                    <TouchableOpacity style={styles.ButtonLogin}>
                        <Text style={styles.ButtonText}>Login</Text>
                    </TouchableOpacity>
                    <Text style={styles.FormText}>or</Text>
                    <TouchableOpacity style={styles.ButtonDaftar}>
                        <Text style={styles.ButtonText}>Register</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 20
    },
    Gambar:{
        width: 300,
        height: 50,
    },
    Title: {
        fontSize: 24,
        color: '#003366',
        marginTop: 20,
        fontWeight: 'bold'
    },
    FormText: {
        fontSize: 20,
        color: '#003366',
        marginHorizontal: 40,
        marginTop: 20
    },
    Form: {
        borderColor: '#003366',
        borderWidth: 0.5,
        height: 40,
        marginHorizontal: 40,
        marginVertical: 10
    },
    ButtonLogin: {
        marginTop: 20,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 200,
        borderRadius: 20
    },
    ButtonDaftar:{
        marginTop: 20,
        backgroundColor: '#003366',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        width: 200,
        borderRadius: 20
    },
    ButtonText: {
        color: 'white',
        fontSize: 20
    }
})
