import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Button,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar
} from 'react-native';

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { AuthContext } from "./context";
const Drawer = createDrawerNavigator();
const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);
export const loginScreen = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);
  return (
  <ScreenContainer>
    <ScrollView>
    <View style={styles.container}>
    <StatusBar 
    backgroundColor = {"grey"}
    translucent = {false}/>
      <View style={styles.header}>
        <Image source={require('./logo.png')} style={{ width: 360, height: 55 }} />
      </View>
      <View style={styles.cheader}><Text style={{fontWeight:'bold'}}>Form Login</Text></View>
      <View style={styles.content}>
        <Text style={{marginLeft:25}}>Email</Text>
        <View style={styles.textBox}></View>
        <Text style={{marginLeft:25}}>Password</Text>
        <View style={styles.textBox}></View>
        <View style={styles.buttonArr}>
          <View style={styles.button}>
            <Text style={styles.text}>Masuk</Text>
          </View>
        </View>
      </View>
      <View style={styles.footer}>
        <Text style={{fontWeight:'bold'}}></Text>
      </View>
    </View>
    </ScrollView>
  </ScreenContainer>
  )
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  header: {
  	marginTop:0,
  	height: 120,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#57bce7',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:400,
  },
  content:{
  	height:435,
  	padding:20
  },
  textBox:{
  	width:300,
  	height:40,
  	marginLeft:25,
  	borderWidth:1,
  	borderStyle:'solid',
  	borderColor:'grey',
  	marginBottom:20
  },
  button:{
    borderWidth:1,
    width:100,
    height:50,
    borderRadius:20,
    backgroundColor:'#036'
  },
  buttonArr:{
  	width:300,
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft:25
  },
  text:{
  	textAlign:'center',
  	marginTop:13,
  	color:'white',
  	fontWeight:'bold'
  },
  footer:{
  	marginTop:50,
  	height: 60,
    backgroundColor: '#57bce7',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});