import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';

import {SignIn, CreateAccount } from './Screens';

const AuthStack = createStackNavigator();

export default function App() {
  return (
    <Navigationcontainer}>
      <AuthStack.Naviator>
        <AuthStack.Screen name="SignIn" component={SignIn} options={{ title: 'Sign In'}}/>
        

      </AuthStack.Naviator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
