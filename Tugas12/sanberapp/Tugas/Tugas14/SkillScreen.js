import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity, FlatList, TextInput,ScrollView} from 'react-native';

import data from './skillData.json';
import ListSkill from './ListSkill';

export default class App extends React.Component{

  render(){
    return(
        <View style={styles.container}>
            <View style={styles.boxLogo}>
                 <Image source={require('./asset/logo.png')} style={{width:187.5, height:51}} />
            </View>
            <View style={styles.boxProfile}>
                <View>
                    <Image source={require('./asset/images.jpg')} style={{width:26, height:26, borderRadius:25 }} />
                </View>
                <View style={{paddingLeft:8}}>
                    <Text style={{fontSize:14}}>
                        Hai,
                    </Text>
                    <Text style={{fontSize:16,color:'#003366'}}>
                        Kevin Claudio Darmawan
                    </Text>
                </View>
            </View>
            <View style={{paddingLeft:16,paddingTop:16}}>
                     <Text style={{fontSize:36,color:'#003366'}}>
                        SKILL
                    </Text>
                    <View style={{width:333,height:4,backgroundColor:'#3EC6FF',border:1}} />
            </View>
            <View style={{paddingLeft:16,paddingTop:5,flexDirection:"row",justifyContent:"space-evenly"}}>
                <View style={styles.listBox}>
                    <Text style={styles.textBox}>
                        Library / Framework
                    </Text>
                </View>
                <View style={styles.listBox}>
                    <Text style={styles.textBox}>
                        Bahasa Pemrograman
                    </Text>
                </View>
                <View style={styles.listBox}>
                    <Text style={styles.textBox}>
                       Teknologi
                    </Text>
                </View>
            </View>
        <ScrollView>
        <FlatList 
          data={data.items}
          renderItem={(skil)=><ListSkill skil={skil.item} />}
          keyExtractor={(item)=>item.id}
        />
        </ScrollView>
       </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  boxLogo:{
      paddingLeft:187,
      paddingTop:10,
  },
  boxProfile:{
      paddingLeft:19,
      flexDirection:"row",
      width:150,
  },
  listBox:{
    height:32,backgroundColor:'#B4E9FF',borderRadius:8,alignItems:"center",
  },
  textBox:{
    color:'#003366',
    fontWeight:'bold',
    margin:7
  }
})
