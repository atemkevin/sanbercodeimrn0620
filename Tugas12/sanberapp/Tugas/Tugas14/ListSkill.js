import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ListSkill extends React.Component{
    render(){ 
        let skil = this.props.skil;
        return (
            <View style={styles.container}>
                 <View style={styles.boks}>
                    <View style={styles.bokisi}>
                        <View>
                            <Icon name={skil.iconName} size={90} style={styles.icon}></Icon>
                        </View>
                        <View>
                            <Text style={{fontSize:24,color: '#003366',fontWeight:"bold",}}>
                                {skil.skillName}
                            </Text>
                            <Text  style={{fontSize:16,color: '#3EC6FF',fontWeight:"bold",}}>
                                {skil.categoryName}
                            </Text>
                            <Text style={{fontSize:48,color: '#FFF',fontWeight:"bold",textAlign:"right"}} >
                                {skil.percentageProgress}
                            </Text>
                        </View>
                        <View>
                            <Icon name="chevron-right" size={90} style={styles.icon}></Icon>
                        </View>
                    </View>
                 </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginLeft:16,
        marginTop:10,
    },
    boks:{
        width:333,
        height:129,
        backgroundColor:'#B4E9FF',
        borderRadius:8,
        elevation:5,
        },
    icon:{
        marginTop:18,
        marginBottom:15,
    },
    bokisi:{
        marginLeft:10,
        marginRight:5,
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:"center"
    }
});
