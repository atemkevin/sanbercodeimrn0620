
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import QuizApp from './Quiz3/index'

export default function App() {
  return (
<QuizApp/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
