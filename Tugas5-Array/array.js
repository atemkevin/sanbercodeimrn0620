console.log("--------------------------------------")
function range(startNum=0,finishNum=0){ 
if(startNum==0 || startNum>0 && finishNum==0){
    return -1;
}else
{
        var number=[];
        if(startNum>finishNum){
            while(startNum >= finishNum){
                number.push(startNum);
                startNum--; 
                }
        }else{
            while(finishNum >= startNum){
            number.push(startNum);
            startNum++; 
            }
        }
        return number;   
    }
}

console.log(range(1, 10))
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 

console.log("---------------------------------------")
function rangeWithStep(startNum,finishNum,Step){
    var number=[];
    var number=[];
    if(finishNum<startNum){
        while(finishNum <= startNum){
            number.push(startNum);
            startNum-=Step; 
            }
    }else{
        while(startNum <= finishNum){
        number.push(startNum);
        startNum+=Step; 
        }
    }
    return number;   
}
 
console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))
console.log("-------------------------------------")
function sum(startNum,finishNum=0,Step=1){
    var number=[];
    var total=0;
    if(finishNum<startNum){
        while(finishNum <= startNum){
            number.push(startNum);
            total+=startNum;
            startNum-=Step; 
            
            }
    }else{
        while(startNum <= finishNum){
        number.push(startNum);
        total+=startNum;
        startNum+=finishNum; 
        }
    }
    return total;   
}

console.log(sum(1,10)) 
console.log(sum(5, 50, 2)) 
console.log(sum(15,10)) 
console.log(sum(20, 10, 2)) 
console.log(sum(1)) 
console.log(sum())
console.log("--------------------------------------")

function dataHandling(a){
    var x=a.length;
    while(x>=1){
        y=a[a.length -x];
        console.log("Nomor ID : "+y[0]);
        console.log("Nama Lengkap : "+y[1]);
        console.log("TTL : "+y[2]+", "+y[3]);
        console.log("Hobby : "+y[4]);
        console.log('');
        x--;
    }
}

var input = [ ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]] 

dataHandling(input);
console.log("-------------------------------------")


function balikKata(a){
    var x=1;
    kata="";
    while(x<=a.length){
        kata+=a[a.length -x];
        x++;
    }
    return kata;
}
console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers"))

console.log('--------------------------------------------')

function dataHandling2(a){
    nama_awal=a[1];
    a.splice(1, 1, "Roman Alamsyah Elsharawy")
    a.splice(2, 1, "Provinsi Bandar Lampung")
    a.splice(4,1, "Pria")
    a.push("SMA Internasional Metro")
    console.log(a);

    var irish=a.slice(3,4);
    var splitt=irish[0].split("/");
    bulan=splitt[1];
    tahun=splitt[2];
    tanggal=splitt[0];
    namaBulan="";
    switch(bulan) {
        case '01':   { namaBulan='Januari'; break; }
        case '02':   { namaBulan='Februari'; break; }
        case '03':   { namaBulan='Maret'; break; }
        case '04':   { namaBulan='April'; break; }
        case '05':   { namaBulan='Mei'; break; }
        case '06':   { namaBulan='Juni'; break; }
        case '07':   { namaBulan='Juli'; break; }
        case '08':   { namaBulan='Agustus'; break; }
        case '09':   { namaBulan='September'; break; }
        case '10':  { namaBulan='Oktober'; break; }
        case '11':  { namaBulan='Nopember'; break; }
        case '12':  { namaBulan='Desember';break; }
        default:  { b=0; console.log('bulan harus diisi antara 1 - 12'); }}
    console.log(namaBulan);
    splitt.pop();
    splitt.unshift(tahun);
    console.log(splitt);
    console.log(tanggal+"-"+bulan+"-"+tahun);
    console.log(nama_awal);
    

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

