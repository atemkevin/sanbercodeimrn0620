function arrayToObject(arr) {
    if(!arr.length) {
        console.log("");
        return ""
    };
    const currentYear = (new Date()).getFullYear();
    const reduced = arr.reduce((a,b) => {
        const [firstName, lastName, gender, birthYear ] = b;
        const key = `${firstName} ${lastName}`;
        const age = !birthYear || birthYear > currentYear ? "Invalid birth year" : currentYear - birthYear;
        const obj = {
            firstName, lastName, gender, age
        }
        a[key] = obj;
        return a;
 },{});
    Object.keys(reduced).forEach((key, index) => {
        console.log(index + 1 + '. ' + key + ' : ', reduced[key])
 });
    return reduced;
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([])

console.log("================================================");

function shoppingTime(memberId, money) {
    const products = [
        {name: 'Sepatu Stacattu', price: 1500000},
        {name: 'Baju Zoro', price: 500000},
        {name: 'Baju H&N', price: 250000},
        {name: 'Sweater Uniklooh', price: 175000},
        {name: 'Casing Handphone', price: 50000},
    ].sort((a,b) => a.price === b.price ? 0 : a.price > b.price ? -1 : 1);

    if(!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if(money < products[products.length -1].price) return "Mohon maaf, uang tidak cukup";

    return products.reduce((a,b) => {
        if(a.changeMoney >= b.price) {
            a.listPurchased.push(b.name);
            a.changeMoney -= b.price;
        }
        return a;
    }, {memberId, money, listPurchased: [], changeMoney: money })
    
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

console.log("--------------------------------------------------------------");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    const cost = 2000;
    return arrPenumpang.reduce((a,b) => {
        const [penumpang, naikDari, tujuan] = b;
        if(penumpang && naikDari && tujuan) {
            const bayar = (rute.indexOf(tujuan) - rute.indexOf(naikDari)) * cost;
            a.push({penumpang, naikDari, tujuan, bayar});
        }
        return a;
    }, [])
  }
   
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
   
  console.log(naikAngkot([]));
