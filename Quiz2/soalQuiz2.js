class Score  {
    constructor({ subject, points, email}) {
        this._subject = subject;
        this._points = points.length ? [...points] : [points];
        this._email = email;
    }

    get subject() {
        return this._subject;
    }

    get email() {
        return this._email;
    }

    get points() {
        return this._points;
    }

    set points(p) {
        this._points = p.length ? [...p] : [p];
    }

    average() {
        return this.sum() / this._points.length;
    }

    sum() {
        return this._points.reduce((a,b) => a + b, 0);
    }
    
}
console.log("---------------------------------------");
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  const [keys, ...datas] = data;
    const object =  datas.reduce((a,[email, ...scores]) => {
      let pointIndex = keys.indexOf(subject);
      if(pointIndex === -1) pointIndex = keys.indexOf(subject.replace('-', ' - '));
      return [...a, { email, subject, points: scores[pointIndex - 1]}]
    }, []);
    console.log(object);
    return object;
}

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

console.log("---------------------------------------");
function recapScores(data) {
const [_, ...datas] = data;

    const sum       = x => x.reduce((a,b) => a + b, 0);
    const avg       = x => sum(x) / x.length;
    const predicate = x => avg(x) > 90 ? 'honour' : avg(x) > 80 ? 'graduate' : avg(x) > 70 ? 'participant' : null; // no condition given for <= 70
    const round     = x => Math.round(x * 10) / 10;
    const objects = datas.map(([email, ...scores]) => ({email, avg: avg(scores), predicate: predicate(scores) }));
    objects.forEach((o,i) => {
      console.log(`${i + 1}. Email: ${o.email}\nRata-rata: ${round(o.avg)}\nPredikat: ${o.predicate}\n`);
    })
    return objects
}

recapScores(data);
